from dbmodel import Controller, Beacon, Position, Location, BeaconPositionHistory, MqttHistory, Sensor, RssiHistory, ControllerStatusHistory, Device
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datetime
from utils import convertPos
from sqlalchemy import exc

## INSERT ##
def insertController(name, desc, active, mac_address, p, os, model, scan_on, beacon_on, ip_address, session):
    try:
        r = Controller(name = name,
                    short_description = desc,
                    active = active,
                    type = 'controller',
                    mac_address = mac_address,
                    position = p,
                    rp_os = os,
                    rp_model = model,
                    scanning_on = scan_on,
                    beacon_on = beacon_on, ip_address = ip_address)
        session.add(r)
        session.commit()
    except:
        session.rollback()
    finally:
        session.close()

def insertBeacon(name, desc, active, mac_address,major, minor, rssi_1m, vendor, info,uuid, br_power, adv_msc, max_range, session):
    try:
        b = Beacon(name =name,
               short_description = desc,
               active = active,
               type='beacon',
               mac_address = mac_address,
               major = major,
               minor = minor,
               rssi_1m = rssi_1m,
               uuid = uuid,
               broadcast_power = br_power,
	           advertise_msc = adv_msc,
  	           max_range = max_range,
               vendor = vendor,
               additional_information = info,
               )
    
        session.add(b)
        session.commit()
    except:
        session.rollback()
    finally:
        session.close()
    
    
def insertSensor(active, name, desc, p, vendor, subt, session):
    try:
        s = Sensor(active = active,
               name = name,
               short_description = desc,
               type = 'sensor',
               position = p,
               vendor = vendor,
               sub_type = subt)
        session.add(s)
        session.commit()
    except:
        session.rollback()
    finally:
        session.close()

def insertLocation(building, floor, office, session):
    try:
        l = Location (building = building,
                  floor = floor,
                  office = office)
        session.add(l)
        session.commit()
        return l.idLocation
    except:
        session.rollback()
        return None
    finally:
        session.close()
        return None

def insertPosition(l, posx, posy, session):
    try:
        pixx, pixy = convertPos(posx, posy)
        p = Position (location = l,
                  posx = posx,
                  posy = posy,
                  pixx = pixx,
                  pixy = pixy)
        session.add(p)
        session.commit()
    except exc.SQLAlchemyError:
        raise
        session.rollback()
    finally:
        session.close()


def insertMqttHistory(mac_address, broker_uptime, msent_1min, msent_upt, mrecv_1min, mrecv_upt, subs_count, cl_conn, session):
    try:
        c = session.query(Controller).filter(Controller.mac_address == mac_address).first()
        m = MqttHistory(controller = c,
                        timestamp = datetime.datetime.now(),
                        broker_uptime = broker_uptime,
                        msgs_sent_1min = msent_1min,
                        msgs_recv_1min = mrecv_1min,
                        ent_uptime = msent_upt,
                        ecv_uptime = mrecv_upt,
                        iptions_count = subs_count,
                        clients_connected = cl_conn)
        session.add(m)
        session.commit()
    except:
        session.rollback()
    finally:
        session.close()

def insertRssiHistory(beacon_mac, rp_mac, rssi_value, distance, session):
     try:
         b = session.query(Beacon).filter(Beacon.mac_address==beacon_mac).first()
         c = session.query(Controller).filter(Controller.mac_address==rp_mac).first()
         r = RssiHistory(beacon = b,
                        controller = c,
                        rssi_value = rssi_value,
                        distance = distance,
                        timestamp = datetime.datetime.now())
        
         session.add(r)
         session.commit()
     except:
        session.rollback()
     finally:
        session.close()
           
        
def insertBeaconPositionHistory(beacon_mac, controller_macs, rssis, distances, posx, posy, cf, session):
    try:
        b = session.query(Beacon).filter(Beacon.mac_address==beacon_mac).first()
        l = session.query(Location).first()
        insertPosition(l, posx, posy, session)
        p = session.query(Position).order_by(Position.idPosition.desc()).first()
        controllers = []
        controller_macs = controller_macs.split(',')
        rssis = rssis.split(',')
        distances = distances.split(',')
        for mac in controller_macs:
            cont_id = session.query(Controller.idController).filter(Controller.mac_address==mac).limit(1).first()
            controllers.append(cont_id.idController)
        cs, rs, ds = map(list, zip(*sorted(zip(controllers, rssis, distances))))
        print(cs)
        print(rs)
        print(ds)
        
        controllers_str = ''
        distances_str = ''
        rssis_str = ''

        for i in range(0,len(cs)):
            controllers_str = controllers_str + str(cs[i]) +','
            distances_str = distances_str + str(ds[i]) + ','
            rssis_str = rssis_str + str(rs[i]) + ','
        
        controllers_str = controllers_str[:-1]
        distances_str = distances_str[:-1]
        rssis_str = rssis_str[:-1]

        print(controllers_str)
        print(distances_str)
        print(rssis_str)

            
        bph = BeaconPositionHistory(beacon = b,
                                    position = p,
                                    controllers = controllers_str,
                                    rssis = rssis_str,
                                    distances=distances_str,
                                    correction_factor = cf,
                                    timestamp = datetime.datetime.now())
        session.add(bph)
        session.commit()
    except exc.SQLAlchemyError:
        raise
        session.rollback()
    finally:
        session.close()
       
def insertControllerStatusHistory(cpu_avg, mem_used, mem_aval, b_sent, b_recv, uptime, processes, mac_address, session):
    
    try: 
        c = session.query(Controller).filter(Controller.mac_address == mac_address).first()
        csh = ControllerStatusHistory (cpu_avg = cpu_avg,
                                         mem_used = mem_used,
                                         mem_aval = mem_aval,
                                         bytes_sent = b_sent,
                                         bytes_recv = b_recv,
                                         uptime = uptime,
                                         processes_running = processes,
                                         controller = c,
                                         timestamp = datetime.datetime.now())
        session.add(csh)
        session.commit()
    except:    
            session.rollback()
    finally:
        session.close()
    
  
#engine = create_engine(
#    'mysql://{username}:{password}@/{db_name}?charset=utf8'
#    '&unix_socket=/cloudsql/{instance_connection_name}'
#    .format(username='root', password='5#WqhcqJ', db_name='bleat_new',
#            instance_connection_name='asset-tracking-183315:us-central1:asset-tracking-db'))  
#Session = sessionmaker(bind=engine)
#session = Session()
#devices = session.query(Device).all()
#  
#result = devicesschema.dump(devices)    
#print( jsonify({'devices':result.data}))  

#beaconpositionsschema = BeaconPositionHistorySchema(many=True)
#beaconpositionschema = BeaconPositionHistorySchema()
#controllerdataschema = ControllerDataSchema(many=True)
#controllerschema = ControllerSchema()

#c = session.query(Controller).first()

#bph = session.query(BeaconPositionHistory).options(joinedload('controllerdata')).first()
#print(bph.controllerdata)

#for c in bph.controllerdata:
#    print(c.idController)
#    c = session.query(Controller).filter(Controller.idController==c.idController)
#    res,err = controllerschema.dump(c)
#    print(res)
#    break

#insertLocation('Brace Ribnikar 56','2','VizLore-office1',session)
#insertLocation('Brace Ribnikar 56','2','VizLore-office2',session)

#l = session.query(Location).first()
#insertPosition(l, 6.6, 8.9, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertSensor(1, 'Enocean contact sensor', 'Checks contact', p ,'Enocean Tech', 'contact', session)
#
#insertPosition(l, 11.6, 12.9, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertSensor(1, 'Enocean temp sensor', 'Checks inside temperature', p,'Enocean Tech', 'temperature', session)
#
#insertPosition(l, 9.6, 14.9, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertSensor(1, 'Enocean switch 1', 'Checks contact', p,'Enocean Tech', 'contact', session)
#
#insertPosition(l, 11.18, 6.60, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertController('Scanner1_office1', 'BLE Scanner', 1,'00:0f:60:05:b5:82', p, 'wiocean','2',1,1, '192.168.0.96', session)
##
#insertPosition(l,6.08, 3.4, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertController('Scanner2_office1', 'BLE Scanner', 1,'00:0f:13:39:22:58', p, 'uioe','2',1,1, '192.168.0.53', session)
#
#insertPosition(l,14.7, 4.4, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertController('Scanner3_office1', 'BLE Scanner', 1,'b2:17:bb:aa:7d:e2', p, 'wiocean','3',1,1, '192.168.0.52', session)
#
#insertPosition(l, 14.7, 4.4, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertController('Scanner4_office2', 'BLE Scanner - VLF099', 1,'b8:27:eb:3b:13:78', p, 'uioe','3',1,1, '192.168.0.36', session)
#
#insertPosition(l, 2.3, 4.3, session)
#p = session.query(Position).order_by(Position.idPosition.desc()).first()
#insertController('Scanner5_office2', 'BLE Scanner - VLF078', 1,'b8:27:eb:26:62:c3', p, 'uioe','3',1,1, '192.168.0.36', session)
#
#insertBeacon('Bicycle nearable yellow', 'Bicycle nearable yellow', 1, 'f4:31:a8:ac:55:fa',200, 202, -67, 'Estimote', 'Bicycle nearable yellow', 'D0D3FA86-CA76-45EC-9BD9-6AF4B043DCB5', 0, 500, 7, session)
#
#insertBeacon('Bag nearable blueberry', 'Bag nearable blueberry', 1, 'c3:76:07:33:3b:59',200, 207, -67, 'Estimote', 'Bag nearable blueberry', 'D0D3FA86-CA76-45EC-9BD9-6AF4CB216677', 0, 500, 7, session)
#
#insertBeacon('Dog neareble pink', 'Dog neareble pink', 1, 'f9:2e:30:c4:f5:de',200, 204, -67, 'Estimote', 'Dog nearable pink', 'B9407F30-F5F8-466E-AFF9-25556B57FE6D', 0, 500, 7, session)
#
#insertBeacon('Laptop nearable blue', 'Laptop nearable blue', 1, 'f9:2e:30:c4:f5:de',200, 206, -67, 'Estimote', 'Laptop nearable blue', 'D0D3FA86-CA76-45EC-9BD9-6AF47A87D053', 0, 500, 7, session)
#
#insertBeacon('TV nearable blue', 'TV nearable blue', 1, 'd0:6c:32:8d:c0:e2',200, 205, -67, 'Estimote', 'TV nearable blue', 
#             'D0D3FA86-CA76-45EC-9BD9-6AF47A87D053', 0, 500, 7, session)
#
#


#
#insertBeacon('Scanner1_office1', 'BLE Scanner', 1,'00:0f:60:05:b5:82',300, 301, -64, 'RaspberryPi', 'RaspberryPi 2 - BL dongle', 
#             'c76faac7-6882-4614-8451-73f081282435', 0, 2000, 30, session)
#
#insertBeacon('Scanner2_office1', 'BLE Scanner', 1,'00:0f:13:39:22:58',300, 302, -65, 'RaspberryPi', 'RaspberryPi 2 - BL dongle', 
#             'ecfa8177-9553-48bd-81b3-593ff2299eda', 0, 2000, 30, session)
#
#insertBeacon('Scanner3_office1', 'BLE Scanner', 1,'b2:17:bb:aa:7d:e2',300, 303, -66, 'RaspberryPi', 'RaspberryPi 3', 
#             '925e80f9-358e-4fbf-822b-32d16b487e88', 0, 2000, 30, session)
#
#insertBeacon('Scanner4_office2', 'BLE Scanner - VLF099', 1,'b8:27:eb:3b:13:78',300, 304, -66, 'RaspberryPi', 'RaspberryPi 3', 
#             '6c2a3641-858f-4c03-87cd-7c022c1851f6', 0, 2000, 30, session)
#
#insertBeacon('Scanner5_office2', 'BLE Scanner - VLF078', 1,'b8:27:eb:26:62:c3',300, 305, -66, 'RaspberryPi', 'RaspberryPi 3', 
#             '7fdf9282-7894-4a4e-970a-bc81c7eea7a7', 0, 2000, 30, session)


#bphs = session.query(BeaconPositionHistory).join(BeaconPositionHistory.controllerdata).filter(ControllerData.idController in [4,5,6])
#for bp in bphs:
#    print(bp.idBeaconPositionHistory)
#filter(BeaconPositionHistory.idBeacon == 15)

#insertBeaconPositionHistory('d0:6c:32:8d:c0:e2', 'b8:27:eb:3b:13:78,00:0f:13:39:22:58,00:0f:60:05:b5:82', '-50,-67.8,-66', '1,7,4', 2, 2, 0.2, session)