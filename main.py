from sqlalchemy import func
import statistics
import time
import base64
import os
import logging
import datetime
from flask import Flask, jsonify, request
from six.moves import http_client
from dbmodel import Position, PositionSchema, BeaconPositionHistory, BeaconPositionHistorySchema, ControllerStatusHistorySchema, ControllerStatusHistory, RssiHistory, RssiHistorySchema, MqttHistorySchema, ControllerSchema, BeaconSchema, DeviceSchema, LocationSchema, SensorSchema, Beacon, Device, Sensor, Location, Controller, MqttHistory
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import Bundle

app = Flask(__name__)

def initSession():
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db = SQLAlchemy(app)
    return db.session

def closeSession(session):
    session.close()
    
devices_schema = DeviceSchema(many=True)
beacons_schema = BeaconSchema(many=True)
location_schema = LocationSchema()
locations_schema = LocationSchema(many=True)
sensor_schema = SensorSchema()
sensors_schema = SensorSchema(many=True)
rpis_schema = ControllerSchema(many=True)
rssi_historys_schema = RssiHistorySchema(many=True)
mqtt_historys_schema = MqttHistorySchema(many=True)
status_historys_schema = ControllerStatusHistorySchema(many=True)
beacon_position_historys_schema = BeaconPositionHistorySchema(many=True)
beacon_position_schema = BeaconPositionHistorySchema()
position_schema = PositionSchema()
controller_schema = ControllerSchema()
#ERRORS
ERROR_PARAM = 'No results.'

def _base64_decode(encoded_str):
    # Add paddings manually if necessary.
    num_missed_paddings = 4 - len(encoded_str) % 4
    if num_missed_paddings != 4:
        encoded_str += b'=' * num_missed_paddings
    return base64.b64decode(encoded_str).decode('utf-8')

@app.route('/echo', methods=['POST'])
def echo():
    """Simple echo service."""
    message = request.get_json().get('message', '')
    return jsonify({'message': message})

@app.route('/get_devices', methods=['GET'])
def getDevices():
    session = initSession()
    devices = session.query(Device).all()
    if len(devices)==0:
        return jsonify({'devices':'No results'})  
    result = devices_schema.dump(devices)    
    return jsonify({'devices':result.data})  
    
@app.route('/get_locations', methods=['GET'])
def getLocations():
    session = initSession()
    locs = session.query(Location).all()
    if len(locs)==0:
        return jsonify({'locations':'No results'})  
    result = locations_schema.dump(locs)    
    return jsonify({'locations':result.data}) 

@app.route('/get_beacons', methods=['GET'])
def getBeacons():
    session = initSession()
    beacons = session.query(Beacon).all()
    if len(beacons)==0:
        return jsonify({'beacons':'No results'})  
    result = beacons_schema.dump(beacons)    
    return jsonify({'beacons':result.data})
    
@app.route('/get_controllers', methods=['GET'])
def getControllers():
    session = initSession()
    rpis = session.query(Controller).all()
    if len(rpis)==0:
        return jsonify({'controllers':'No results'})  
    result = rpis_schema.dump(rpis)    
    return jsonify({'controllers':result.data})

@app.route('/get_sensors', methods=['GET'])
def getSensors():
    session = initSession()
    sensors = session.query(Sensor).all()
    if len(sensors)==0:
        return jsonify({'sensors':'No results'})  
    result = sensors_schema.dump(sensors)    
    return jsonify({'sensors':result.data})

@app.route('/get_mqtt_history', methods=['POST'])
def getMqttHistory():
    session = initSession()
    mh = 0
    try:
        #mora format ovakav: 1988-01-17, 
        #ako ne zelimo da ukljucimo date range u pretragu, moramo proslediti ipak prazan string
        #ako necemo max results prosledimo 0
        rpid = request.get_json().get('controllerId', '')
        from_date = request.get_json().get('from_date', '')
        to_date = request.get_json().get('to_date', '')
        max_results = request.get_json().get('max_results', '')
        if (from_date != '' and to_date != '' and max_results!=0):
            mh = session.query(MqttHistory).filter(MqttHistory.idController == int(rpid)).filter(MqttHistory.timestamp.between(from_date, to_date)).order_by(MqttHistory.timestamp.desc()).limit(max_results).all()
        if(from_date != '' and to_date != '' and max_results==0):
            mh = session.query(MqttHistory).filter(MqttHistory.idController == int(rpid)).filter(MqttHistory.timestamp.between(from_date, to_date)).order_by(MqttHistory.timestamp.desc()).all()
        if (from_date == '' and to_date == '' and max_results==0):
            mh = session.query(MqttHistory).filter(MqttHistory.idController == int(rpid)).order_by(MqttHistory.timestamp.desc()).all()
        if (from_date == '' and to_date == '' and max_results!=0):
            mh = session.query(MqttHistory).filter(MqttHistory.idController == int(rpid)).order_by(MqttHistory.timestamp.desc()).limit(max_results).all()
    except:
        return jsonify({'Server error':ERROR_PARAM})  
        
    if len(mh)==0:
        return jsonify({'mqtt_history':'No results'})  
    result = mqtt_historys_schema.dump(mh)    
    return jsonify({'mqtt_history':result.data})

@app.route('/get_rssi_history', methods=['POST'])
def getRssiHistoryRp():
    session = initSession()
    rssihis = 0
    try:
        rpid = request.get_json().get('controllerId', '')
        from_date = request.get_json().get('from_date', '')
        to_date = request.get_json().get('to_date', '')
        max_results = request.get_json().get('max_results', '')
        if (from_date != '' and to_date != '' and max_results!=0):
            rssihis = session.query(RssiHistory).filter(RssiHistory.idController == int(rpid)).filter(RssiHistory.timestamp.between(from_date, to_date)).order_by(RssiHistory.timestamp.desc()).limit(max_results).all()
        if(from_date != '' and to_date != '' and max_results==0):
            rssihis = session.query(RssiHistory).filter(RssiHistory.idController == int(rpid)).filter(RssiHistory.timestamp.between(from_date, to_date)).order_by(RssiHistory.timestamp.desc()).all()
        if (from_date == '' and to_date == '' and max_results==0):
            rssihis = session.query(RssiHistory).filter(RssiHistory.idController == int(rpid)).order_by(RssiHistory.timestamp.desc()).all()
        if (from_date == '' and to_date == '' and max_results!=0):
            rssihis = session.query(RssiHistory).filter(RssiHistory.idController == int(rpid)).order_by(RssiHistory.timestamp.desc()).limit(max_results).all()
    except: 
        return jsonify({'Server error':ERROR_PARAM})  

    if len(rssihis) == 0:
        return jsonify({'rssi_history':'No results'})  
    result = rssi_historys_schema.dump(rssihis)    
    return jsonify({'rssi_history':result.data})

@app.route('/get_status_history', methods=['POST'])
def getRpStatusHistory():
    session = initSession()
    sh = 0
    try:
        rpid = request.get_json().get('controllerId', '')
        from_date = request.get_json().get('from_date', '')
        to_date = request.get_json().get('to_date', '')
        max_results = request.get_json().get('max_results', '')
        if (from_date != '' and to_date != '' and max_results!=0):
            sh = session.query(ControllerStatusHistory).filter(ControllerStatusHistory.idController == int(rpid)).filter(ControllerStatusHistory.timestamp.between(from_date, to_date)).order_by(ControllerStatusHistory.timestamp.desc()).limit(max_results).all()
        if(from_date != '' and to_date != '' and max_results==0):
            sh = session.query(ControllerStatusHistory).filter(ControllerStatusHistory.idController == int(rpid)).filter(ControllerStatusHistory.timestamp.between(from_date, to_date)).order_by(ControllerStatusHistory.timestamp.desc()).all()
        if (from_date == '' and to_date == '' and max_results==0):
            sh = session.query(ControllerStatusHistory).filter(ControllerStatusHistory.idController == int(rpid)).order_by(ControllerStatusHistory.timestamp.desc()).all()
        if (from_date == '' and to_date == '' and max_results!=0):
            sh = session.query(ControllerStatusHistory).filter(ControllerStatusHistory.idController == int(rpid)).order_by(ControllerStatusHistory.timestamp.desc()).limit(max_results).all()

    except:    
        return jsonify({'Server error':ERROR_PARAM})  

    if len(sh) == 0:
        return jsonify({'status_history':'No results'})  
    result = status_historys_schema.dump(sh)    
    return jsonify({'status_history':result.data})

@app.route('/get_history', methods=['POST'])
def getHistory():
    try:
        session = initSession()
        
        sh = 0
        mh = 0
        rh = 0
       
        rpid = request.get_json().get('controllerId', '')
        sh = session.query(ControllerStatusHistory).filter(ControllerStatusHistory.idController == int(rpid)).order_by(ControllerStatusHistory.timestamp.desc()).limit(10).all()
        rh = session.query(RssiHistory).filter(RssiHistory.idController == int(rpid)).order_by(RssiHistory.timestamp.desc()).limit(10).all()
        mh = session.query(MqttHistory).filter(MqttHistory.idController == int(rpid)).order_by(MqttHistory.timestamp.desc()).limit(10).all()
            
        result_sh = status_historys_schema.dump(sh)    
        result_rh = rssi_historys_schema.dump(rh)    
        result_mh = mqtt_historys_schema.dump(mh)
        
            
        return jsonify( {'history': { 'mqtt': result_mh.data, 'status' : result_sh.data, 'rssi': result_rh.data} })
    except:
        return jsonify({'Server error':ERROR_PARAM})  
      
@app.route('/get_beacon_position_history', methods=['POST'])
def getBeaconPositionHistory():
    print("HISTORY CALLED")
    try:
        session = initSession()
        bids = request.get_json().get('beaconsId', '')
        from_date = request.get_json().get('from_date', '')
        to_date = request.get_json().get('to_date', '')
        controllers = request.get_json().get('controllers', '')
        fdate = datetime.datetime.strptime(str(from_date),'%Y-%m-%d;%H,%M')
        fdatef = datetime.datetime(fdate.year, fdate.month, fdate.day, fdate.hour, fdate.minute)
        
        tdate = datetime.datetime.strptime(str(to_date),'%Y-%m-%d;%H,%M')
        tdatet = datetime.datetime(tdate.year, tdate.month, tdate.day, tdate.hour, tdate.minute)
         
        controllers1 = controllers.split(',')
        controllers_list = list(map(int, controllers1))
        beacons = bids.split(',')
        beacons_list = list(map(int, beacons))
        beacons_array = []
        controllers_array = []
        for beacon_id in beacons_list:
            beacon = {}
            xpix = []
            ypix = []
            xm = []
            ym = []
            positions = []
            beacon['idBeacon'] = beacon_id
            beacon_name = session.query(Beacon.name).filter(Beacon.idBeacon == beacon_id).first()
            beacon['name'] = beacon_name.name.split(' ')[1]
            bph_b = Bundle('beaconpositionhistory', BeaconPositionHistory.timestamp, BeaconPositionHistory.controllers)
            pos_b = Bundle('position', Position.pixx, Position.pixy, Position.posx, Position.posy)        
            for bph,pos in session.query(bph_b, pos_b).join(BeaconPositionHistory.position).filter(BeaconPositionHistory.idBeacon==beacon_id).filter(BeaconPositionHistory.timestamp.between(fdatef, tdatet)).filter(BeaconPositionHistory.idBeacon == beacon_id).filter(BeaconPositionHistory.controllers == str(controllers)):
                position = {}
                if pos.posx<1:
                        continue
                if pos.posx<8 and pos.posy>6.4:
                    continue
                if pos.posx>15 and pos.posx<16.26:
                    if pos.posy<2.8:
                        continue
                position['posx'] = pos.pixx
                position['posy'] = pos.pixy
#                   
                xpix.append(pos.pixx)
                ypix.append(pos.pixy)
                xm.append(pos.posx)
                ym.append(pos.posy)
            
                position['timestamp'] = bph.timestamp
                pattern = '%Y-%m-%d %H:%M:%S'
                epoch = int(time.mktime(time.strptime(str(bph.timestamp), pattern)))
                position['timestamp_epoch'] = epoch
                positions.append(position)
            
            if(len(xpix)>0 and len(ypix)>0):
                beacon['averageX'] = int(sum(xpix) / len(xpix))
                beacon['averageY'] = int(sum(ypix) / len(ypix))
            else:
                beacon['averageX'] = int(sum(xpix))
                beacon['averageY'] = int(sum(xpix))

            if(len(xm)>1 and len(ym)>1):
                beacon['standardDeviationX'] = statistics.stdev(xm)
                beacon['standardDeviationY'] = statistics.stdev(ym)
            else:
                beacon['standardDeviationX'] = -1
                beacon['standardDeviationY'] = -1
                
            beacon['positions'] = positions
            beacons_array.append(beacon)

        for c_id in controllers_list:
                cont = session.query(Controller).get(c_id)
                controller = {}
                name = cont.name.split('_')[0]
                position = {}
                positions = []
                controller['idController'] = cont.idController
                controller['name'] = name
                controller['posx'] = cont.position.pixx
                controller['posy'] = cont.position.pixy
                controller['beaconOn'] = cont.beacon_on
                controller['scanningOn'] = cont.scanning_on
                controller['ipaddress'] = cont.ip_address
                controller['cpu_type'] = cont.cpu_type
                controller['sdcard'] = cont.sdcard
                controller['ram'] = cont.ram
                beacon = session.query(Beacon).filter(Beacon.mac_address==cont.mac_address).first()
                controller['uuid'] = beacon.uuid
                controller['major'] = beacon.major
                controller['minor'] = beacon.minor
                controller['reference_rssi'] = beacon.rssi_1m
                controller['broadcast_power'] = beacon.broadcast_power
                controller['advertise_msc'] = beacon.advertise_msc
                controller['max_range'] = beacon.max_range
                controllers_array.append(controller)
        return jsonify({'beacons':beacons_array, 'controllers': controllers_array})  
    except:    
        raise
        pass
    finally:
        closeSession(session)
    return jsonify({'Server error':ERROR_PARAM})  
   
  
@app.route('/get_last_beacon_position', methods=['POST'])
def getLastBeaconPosition():
   
    try:
        session = initSession()
        controllers = session.query(Controller.idController, Controller.name, Controller.idPosition, Controller.beacon_on, Controller.scanning_on, Controller.cpu_type, Controller.sdcard, Controller.ip_address, Controller.mac_address, Controller.ram).all();
        controllers_array = []
        for cont in controllers:
            controller = {}
            positions = []
            pos = session.query(Position.pixx, Position.pixy).filter(Position.idPosition==cont.idPosition).first()
            name = cont.name.split('_')[0]
            controller['idController'] = cont.idController
            controller['name'] = name
            controller['posx'] = pos.pixx
            controller['posy'] = pos.pixy
            controller['beaconOn'] = cont.beacon_on
            controller['scanningOn'] = cont.scanning_on
            controller['ipaddress'] = cont.ip_address
            controller['cpu_type'] = cont.cpu_type
            controller['sdcard'] = cont.sdcard
            controller['ram'] = cont.ram
            beacon = session.query(Beacon.uuid, Beacon.major, Beacon.minor, Beacon.rssi_1m, Beacon.broadcast_power, Beacon.advertise_msc, Beacon.max_range).filter(Beacon.mac_address==cont.mac_address).first()
            controller['uuid'] = beacon.uuid
            controller['major'] = beacon.major
            controller['minor'] = beacon.minor
            controller['reference_rssi'] = beacon.rssi_1m
            controller['broadcast_power'] = beacon.broadcast_power
            controller['advertise_msc'] = beacon.advertise_msc
            controller['max_range'] = beacon.max_range
            controllers_array.append(controller)
       
        bids = request.get_json().get('beaconsId', '')
        beacons = bids.split(',')
        beacons_list = list(map(int, beacons))
        beacons_array = []
        
        for beacon_id in beacons_list:
            beacon = {}
            max_id = session.query(func.max(BeaconPositionHistory.idBeaconPositionHistory)).filter(BeaconPositionHistory.idBeacon== beacon_id).scalar()
            if(max_id == None):
                continue
            last_bph = session.query(BeaconPositionHistory).get(max_id)
            to_date = last_bph.timestamp
            from_date = to_date - datetime.timedelta(minutes=2)
            posx = []
            posy = []
            beacon = {}
            positions = []
            position = {}
            
            position['timestamp'] = last_bph.timestamp
            pattern = '%Y-%m-%d %H:%M:%S'
            epoch = int(time.mktime(time.strptime(str(last_bph.timestamp), pattern)))
            position['timestamp_epoch'] = epoch
            beacon_name = session.query(Beacon.name).filter(Beacon.idBeacon == beacon_id).first()
            beacon['name'] = beacon_name.name.split(' ')[1]

            bph_b = Bundle('beaconpositionhistory', BeaconPositionHistory.timestamp)
            pos_b = Bundle('position', Position.pixx, Position.pixy)        
            for bph,pos in session.query(bph_b, pos_b).join(BeaconPositionHistory.position).filter(BeaconPositionHistory.timestamp.between(from_date, to_date)).filter(BeaconPositionHistory.idBeacon==beacon_id):
#                if pos.posx<1:
#                        continue
#                if pos.posx<8 and pos.posy>6.4:
#                    continue
#                if pos.posx>15 and pos.posx<16.26:
#                    if pos.posy<2.8:
#                        continue
                posx.append(pos.pixx)
                posy.append(pos.pixy)
                if(len(posx)>0 and len(posy)>0):
                    position['posx'] = int(sum(posx)/len(posx))
                    position['posy'] = int(sum(posy)/len(posy))
                else:
                    position['posx'] = int(sum(posx))
                    position['posy'] = int(sum(posy))
               
            if position:
                positions.append(position)
            beacon['positions'] = positions
            beacons_array.append(beacon)
        return jsonify({'beacons':beacons_array, 'controllers':controllers_array})  
    except:    
        raise
        pass
    finally:
        closeSession(session)
    return jsonify({'Server error':ERROR_PARAM})  
#
@app.route('/get_last_beacon_position_controllers', methods=['POST'])

def getLastBeaconPositionControllers():
    print('CALLED GET LAST BEACON POSITION CONTROLLERS')
    try:
        session = initSession()
        bids = request.get_json().get('beaconsId', '')
        beacons = bids.split(',')
        controllers = request.get_json().get('controllers', '')
        controllers1 = controllers.split(',')
        controllers_list = list(map(int, controllers1))
        beacons_list = list(map(int, beacons))
        beacons_array = []
        controllers_array = []
       
        for beacon_id in beacons_list:
          
            last_bph= session.query(BeaconPositionHistory).filter(BeaconPositionHistory.idBeacon == beacon_id).filter(BeaconPositionHistory.controllers==controllers).order_by(BeaconPositionHistory.timestamp.desc()).limit(1).first() 
            positions = []
            position = {}
            posx=[]
            posy=[]
            beacon = {}
            beacon_name = session.query(Beacon.name).filter(Beacon.idBeacon == beacon_id).first()
            beacon['name'] = beacon_name.name.split(' ')[1]
            beacon['idBeacon'] = beacon_id
            if(last_bph):
                to_date = last_bph.timestamp
                from_date = to_date - datetime.timedelta(minutes=2)               
                bph_b = Bundle('beaconpositionhistory', BeaconPositionHistory.timestamp)
                pos_b = Bundle('position', Position.pixx, Position.pixy)        
                for bph,pos in session.query(bph_b, pos_b).join(BeaconPositionHistory.position).filter(BeaconPositionHistory.idBeacon==beacon_id).filter(BeaconPositionHistory.timestamp.between(from_date, to_date)).filter(BeaconPositionHistory.controllers==controllers):
                    if last_bph.position.posx<1:
                            continue
                    if last_bph.position.posx<8 and last_bph.position.posy>6.4:
                        continue
                    if last_bph.position.posx>15 and last_bph.position.posx<16.26:
                        if last_bph.position.posy<2.8:
                            continue
                    posx.append(last_bph.position.pixx)
                    posy.append(last_bph.position.pixy)
                    if(len(posx)>0 and len(posy)>0):
                        position['posx'] = int(sum(posx)/len(posx))
                        position['posy'] = int(sum(posy)/len(posy))
                    else:
                        position['posx'] = int(sum(posx))
                        position['posy'] = int(sum(posy))
                    position['timestamp'] = last_bph.timestamp
                    pattern = '%Y-%m-%d %H:%M:%S'
                    epoch = int(time.mktime(time.strptime(str(last_bph.timestamp), pattern)))
                    position['timestamp_epoch'] = epoch
                if position:
                    positions.append(position)

            beacon['positions'] = positions
            beacons_array.append(beacon)

        for c_id in controllers_list:
            cont = session.query(Controller).filter(Controller.idController==c_id).first()
            controller = {}
            controller['idController'] = cont.idController
            name = cont.name.split('_')[0]
            controller['name'] = name
            controller['posx'] = cont.position.pixx
            controller['posy'] = cont.position.pixy
            controller['beaconOn'] = cont.beacon_on
            controller['scanningOn'] = cont.scanning_on
            controller['ipaddress'] = cont.ip_address
            controller['cpu_type'] = cont.cpu_type
            controller['sdcard'] = cont.sdcard
            controller['ram'] = cont.ram
            beacon = session.query(Beacon).filter(Beacon.mac_address==cont.mac_address).first()
            controller['uuid'] = beacon.uuid
            controller['major'] = beacon.major
            controller['minor'] = beacon.minor
            controller['reference_rssi'] = beacon.rssi_1m
            controller['broadcast_power'] = beacon.broadcast_power
            controller['advertise_msc'] = beacon.advertise_msc
            controller['max_range'] = beacon.max_range

            controllers_array.append(controller)
        return jsonify({'beacons':beacons_array, 'controllers': controllers_array })  
    except:    
        raise
        pass
    finally:
        closeSession(session)
    return jsonify({'Server error':ERROR_PARAM})  
    
@app.route('/get_last_positions', methods=['GET'])
def getLastBeaconPositions():
    print("CALLED GET LAST POSITIONS")
    try: 
        session = initSession()
        controllers = session.query(Controller).all();
        controllers_array = []
        for cont in controllers:
            controller = {}
            positions = []
            pixx, pixy = session.query(Position.pixx, Position.pixy).filter(Position.idPosition==cont.idPosition).first()
            name = cont.name.split('_')[0]
            controller['idController'] = cont.idController
            controller['name'] = name
            position = {}
            position['posx'] = pixx
            position['posy'] = pixy
            positions.append(position)
            controller['positions'] = positions
            controller['beaconOn'] = cont.beacon_on
            controller['scanningOn'] = cont.scanning_on
            controller['ipaddress'] = cont.ip_address
            controller['cpu_type'] = cont.cpu_type
            controller['sdcard'] = cont.sdcard
            controller['ram'] = cont.ram
            beacon = session.query(Beacon).filter(Beacon.mac_address==cont.mac_address).first()
            controller['uuid'] = beacon.uuid
            controller['major'] = beacon.major
            controller['minor'] = beacon.minor
            controller['reference_rssi'] = beacon.rssi_1m
            controller['broadcast_power'] = beacon.broadcast_power
            controller['advertise_msc'] = beacon.advertise_msc
            controller['max_range'] = beacon.max_range
            controllers_array.append(controller)
            
        beacons = session.query(Beacon).all();
        beacons_array = []
        
        for bc in beacons:
            max_id = session.query(func.max(BeaconPositionHistory.idBeaconPositionHistory)).filter(BeaconPositionHistory.idBeacon== bc.idBeacon).scalar()
            if(max_id == None):
                continue
            last_bph = session.query(BeaconPositionHistory).get(max_id)
#            to_date = last_bph.timestamp
#            from_date = to_date - datetime.timedelta(minutes=1)
            posx = []
            posy = []
            beacon = {}
            positions = []
            bid = bc.idBeacon
            position = {}
            
#            bph_b = Bundle('beaconpositionhistory', BeaconPositionHistory.timestamp)
#            pos_b = Bundle('position', Position.pixx, Position.pixy)        
#            for bph_b,pos_b in session.query(bph_b, pos_b).join(BeaconPositionHistory.position).filter(BeaconPositionHistory.timestamp.between(from_date, to_date)):
            posx.append(last_bph.position.pixx)
            posy.append(last_bph.position.pixy)
            position['timestamp'] = last_bph.timestamp
            pattern = '%Y-%m-%d %H:%M:%S'
            epoch = int(time.mktime(time.strptime(str(last_bph.timestamp), pattern)))
            position['timestamp_epoch'] = epoch
            beacon['idBeacon'] = bid
            beacon['name'] = bc.name.split(' ')[1]
            if(len(posx)>0 and len(posy)>0):
                position['posx'] = int(sum(posx)/len(posx))
                position['posy'] = int(sum(posy)/len(posy))
            else:
                position['posx'] = int(sum(posx))
                position['posy'] = int(sum(posy))

            if position:
                positions.append(position)
            beacon['positions'] = positions
            beacons_array.append(beacon)
      
        return jsonify({'beacons': beacons_array,'controllers': controllers_array})
    except:
        raise
        pass
    finally:
        closeSession(session)
    return jsonify({'Server error':ERROR_PARAM})  


@app.errorhandler(http_client.INTERNAL_SERVER_ERROR)
def unexpected_error(e):
    """Handle exceptions by returning swagger-compliant json."""
    logging.exception('An error occured while processing the request.')
    response = jsonify({
        'code': http_client.INTERNAL_SERVER_ERROR,
        'message': 'Exception: {}'.format(e)})
    response.status_code = http_client.INTERNAL_SERVER_ERROR
    return response


if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)