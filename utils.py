#converts x y coordinates to pixel height and width
def convertPos(x,y):
    imgw = 1012
    imgh = 441
    maxx = 16.26
    maxy = 6.88
    if x>6 and x<11:
        if y<3:
            y = 3
    if x>11 and x<14:
        if y<2:
            y = 2
    if x>14 and x<15:
        if y<2.3:
            y = 2.3
    if x<15 and x<maxx:
        if y<2.8:
            y = 2.8
    posx_pix = (imgw * x) / maxx
    posy_pix = (imgh * y) / maxy
    return int(posx_pix), int(posy_pix)