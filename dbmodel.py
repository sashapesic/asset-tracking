from sqlalchemy import Column, DateTime, Float,ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base 
from marshmallow import Schema, fields

#engine = create_engine(
#    'mysql://{username}:{password}@/{db_name}?charset=utf8'
#    '&unix_socket=/cloudsql/{instance_connection_name}'
#    .format(username='root', password='5#WqhcqJ', db_name='bleat_new',
#            instance_connection_name='asset-tracking-183315:us-central1:asset-tracking-db'))  
Base = declarative_base()

class Position(Base):
    __tablename__ = 'position'

    idPosition = Column(Integer, primary_key=True)    
    idLocation = Column(ForeignKey('location.idLocation'), nullable=False, index=True)
    posx = Column(Float, nullable=False)
    posy = Column(Float, nullable=False)
    pixx = Column(Integer, nullable=False)
    pixy = Column(Integer, nullable=False)
    
    location = relationship('Location')
       
class Location(Base):
    __tablename__ = 'location'

    idLocation = Column(Integer, primary_key=True)
    building = Column(String(45), nullable=False)
    floor = Column(String(45), nullable=False)
    office = Column(String(45), nullable=False)
    

class Device(Base):
    __tablename__ = 'device'

    idDevice = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(45), nullable=False)
    type = Column(String(45), nullable=False)
    short_description = Column(String(255), nullable=False)
    active = Column(Integer, nullable=False)

    __mapper_args__ = {
        'polymorphic_identity':'device',
        'polymorphic_on':type
    }

class Beacon(Device):
    __tablename__ = 'beacon'

    idBeacon = Column(ForeignKey('device.idDevice'), primary_key=True)
    mac_address = Column(String(45), nullable=False)
    major = Column(Integer, nullable=False)
    minor = Column(Integer, nullable=False)
    rssi_1m = Column(Integer, nullable=False)
    vendor = Column(String(255))
    additional_information = Column(String(45))
    uuid = Column(String(100))
    broadcast_power = Column(Integer)
    advertise_msc = Column (Integer)
    max_range = Column(Integer)

    __mapper_args__ = {
        'polymorphic_identity':'beacon'
    }

    rssidata = relationship('RssiHistory', back_populates='beacon')
    positiondata = relationship('BeaconPositionHistory', back_populates='beacon')

class Controller(Device):
    __tablename__ = 'controller'

    mac_address = Column(String(45), nullable=False)
    idPosition = Column(ForeignKey('position.idPosition'), nullable=False, index=True)
    idController = Column(ForeignKey('device.idDevice'), primary_key=True)
    rp_os = Column(String(255), nullable=False)
    rp_model = Column(String(255), nullable=False)
    scanning_on = Column(Integer, nullable = False)
    beacon_on = Column(Integer, nullable = False)
    ip_address = Column(String(50), nullable = False)
    cpu_type = Column(String(50), nullable=False)
    sdcard = Column(String(50), nullable=False)
    ram = Column(String(50), nullable=False)

    position = relationship('Position')
    statusdata = relationship('ControllerStatusHistory', back_populates='controller')
    mqttstatusdata = relationship('MqttHistory', back_populates='controller')
    rssidata = relationship('RssiHistory', back_populates='controller')
    wifiscandata = relationship('WifiScan',back_populates='controller' )

    __mapper_args__ = {
        'polymorphic_identity':'controller'
        }
    
class WifiScan (Base):
    __tablename__ = 'wifiscan'

    idWifiScan = Column(Integer, primary_key=True, autoincrement=True)
    idController = Column(ForeignKey('controller.idController'), nullable = False, index = True)
    accesspoint_name = Column(String(255), nullable = False)
    conn_no = Column(Integer, nullable = False)
    timestamp = Column(DateTime(), nullable=False)
    period_minutes = Column(Integer, nullable = False)
    
    controller = relationship('Controller', back_populates='wifiscandata')
    footprintdata = relationship('WiFiFootprint', back_populates='wifiscan')

    
class WiFiFootprint(Base):
    __tablename__ = 'wififootprint'

    idWifiFootprint = Column(Integer, primary_key=True, autoincrement=True)
    idWifiScan = Column(ForeignKey('wifiscan.idWifiScan'), nullable = False, index = True)
    ssid = Column(String(200), nullable = False)
    bssid = Column(String(200), nullable = False)
    encryption = Column(String(100), nullable = False)
    channel = Column(Integer, nullable = False)
    quality = Column(Float, nullable = False)
    
    wifiscan = relationship('WifiScan', back_populates='footprintdata')

    
class MqttHistory(Base):
    __tablename__ = 'mqtthistory'

    idController = Column(ForeignKey('controller.idController'), nullable=False, index=True)
    idMqtt_history = Column(Integer, primary_key=True)
    timestamp = Column(DateTime(), nullable=False)
    broker_uptime = Column(Integer, nullable=False)
    msgs_sent_1min = Column(Integer, nullable=False)
    msgs_recv_1min = Column(Integer, nullable=False)
    msgs_sent_uptime = Column(Integer, nullable=False)
    msgs_recv_uptime = Column(Integer, nullable=False)
    subscriptions_count = Column(Integer, nullable=False)
    clients_connected = Column(String(255), nullable=False)
    
    controller = relationship('Controller', back_populates='mqttstatusdata')


class ControllerStatusHistory(Base):
    __tablename__ = 'controllerstatushistory'

    idControllerStatusHistory = Column(Integer, primary_key=True)
    cpu_avg = Column(Float, nullable=False)
    mem_used = Column(Float, nullable=False)
    mem_aval = Column(Float, nullable=False)
    bytes_sent = Column(Integer, nullable=False)
    bytes_recv = Column(Integer, nullable=False)
    uptime = Column(Integer, nullable=False)
    processes_running = Column(Integer, nullable=False)
    idController= Column(ForeignKey('controller.idController'), nullable=False, index=True)
    timestamp = Column(DateTime(), nullable=False)
    
    controller = relationship('Controller', back_populates="statusdata")

class RssiHistory(Base):
    __tablename__ = 'rssihistory'

    idRssi_history = Column(Integer, primary_key=True)
    idBeacon = Column(ForeignKey('beacon.idBeacon'), nullable=False, index=True)
    idController = Column(ForeignKey('controller.idController'), nullable=False, index=True)
    rssi_value = Column(Float, nullable=False)
    distance = Column(Float, nullable=False)
    timestamp = Column(DateTime(), nullable=False)
    
    controller = relationship('Controller', back_populates="rssidata")
    beacon = relationship('Beacon', back_populates='rssidata')

 
class BeaconPositionHistory(Base):
    __tablename__ = 'beaconpositionhistory'

    idBeaconPositionHistory = Column(Integer, primary_key=True)
    idBeacon = Column(ForeignKey('beacon.idBeacon'), nullable=False, index=True)
    idPosition = Column(ForeignKey('position.idPosition'), nullable=False, index=True)
    timestamp = Column(DateTime(), nullable=False)
    correction_factor = Column(Float, nullable = False)
    controllers = Column(String(100), nullable=False)
    rssis =  Column(String(100), nullable=False)
    distances =  Column(String(100), nullable=False)
    beacon = relationship('Beacon', back_populates='positiondata')
    position = relationship('Position')
    
class SensorHistory(Base):
    __tablename__ = 'sensorhistory'

    idSensor_history = Column(Integer, primary_key=True)
    idSensor = Column(ForeignKey('sensor.idSensor'), nullable=False, index=True)
    timestamp = Column(DateTime(), nullable=False)
    value = Column(String(45), nullable=False)
    
    sensor = relationship('Sensor', back_populates='sensordata')
    
class Sensor(Device):
    __tablename__ = 'sensor'

    idSensor = Column(ForeignKey('device.idDevice'), primary_key=True)
    idPosition = Column(ForeignKey('position.idPosition'), nullable=False, index=True)
    vendor = Column(String(45), nullable=False)
    sub_type = Column(String(45), nullable=False)
    
    __mapper_args__ = {
        'polymorphic_identity':'sensor'
    }

    position = relationship('Position')
    sensordata = relationship('SensorHistory', back_populates='sensor')
    

    
class SmartDevice(Device):
    __tablename__ = 'smartdevice'
    
    idSmartDevice = Column(ForeignKey('device.idDevice'), primary_key=True)
    ip_address = Column(String(50), nullable = False)
    
    statusdata = relationship('SmartDeviceStatus', back_populates='smartdevice')
    
    __mapper_args__ = {
        'polymorphic_identity':'smartdevice'
        }
    
class SmartDeviceStatus(Base):
    __tablename__ = 'smartdevicestatus'
    idSmartDeviceStatus = Column(Integer, primary_key=True, autoincrement=True)
    idSmartDevice = Column(ForeignKey('smartdevice.idSmartDevice'), nullable = False)
    accelerometer = Column(Float, nullable = False)
    gyroscope = Column(Float, nullable = False)
    gps_lon = Column(Float, nullable = False)
    gps_lat = Column(Float, nullable = False)
    idPosition = Column(ForeignKey('position.idPosition'), nullable = False)
    timestamp = Column(DateTime(), nullable=False)
    
    position = relationship('Position')
    smartdevice = relationship('SmartDevice', back_populates="statusdata")
#
##### SCHEMAS ####
#    
class PositionSchema(Schema):
    idPosiion = fields.Int(dump_only=True)
    location  = fields.Nested('LocationSchema')
    posx = fields.Float()
    posy = fields.Float()
    pixx = fields.Int()
    pixy = fields.Int()

class LocationSchema(Schema):
    idLocation = fields.Int(dump_only=True)
    building = fields.Str()
    floor = fields.Str()
    office = fields.Str()
    
class DeviceSchema(Schema):
    idDevice = fields.Int(dump_only=True)
    name = fields.Str()
    type = fields.Str()
    short_description = fields.Str()
    active = fields.Int()

class SensorSchema(DeviceSchema):
    idSensor = fields.Int(dump_only=True)
    position  = fields.Nested('PositionSchema')
    vendor = fields.Str()
    sub_type = fields.Str()
    
    sensordata = fields.Nested('SensorHistorySchema', many=True)

class BeaconSchema(DeviceSchema):
    idBeacon = fields.Int(dump_only=True)
    mac_address = fields.Str()
    major = fields.Int()
    minor = fields.Int()
    rssi_1m = fields.Int()
    vendor =fields.Str()
    additional_information = fields.Str()
    uuid = fields.Str()
    broadcast_power = fields.Int()
    advertise_msc = fields.Int()
    max_range = fields.Int()

class ControllerSchema(DeviceSchema):
    idController = fields.Int(dump_only=True)
    mac_address = fields.Str()
    position = fields.Nested('PositionSchema')
    rp_os = fields.Str()
    rp_model = fields.Str()
    scanning_on = fields.Int()
    beacon_on = fields.Int()
    cpu_type = fields.Str()
    ram = fields.Str()
    sdcard = fields.Str()

class WifiScanSchema(Schema):
    idWifiScan = fields.Int(dump_only=True)
    controller = relationship('ControllerSchema')
    accesspoint_name = fields.Str()
    conn_no = fields.Int()
    timestamp =  fields.DateTime()
    period_minutes = fields.Int()
    
    footprintdata = fields.Nested('WiFiFootprintSchema', many=True)

class WiFiFootprintSchema(Schema):

    idWifiFootprint =  fields.Int(dump_only=True)
    wifiscan = fields.Nested('WifiScanSchema')    
    ssid =  fields.Str()
    bssid = fields.Str()
    encryption =  fields.Str()
    channel =  fields.Int()
    quality =  fields.Float()
    
    
class MqttHistorySchema(Schema):

    idMqtt_history = fields.Int(dump_only=True)
#    controller = fields.Nested('ControllerSchema')
    timestamp = fields.DateTime()
    broker_uptime = fields.Int()
    msgs_sent_1min = fields.Int()
    msqs_recv_1min = fields.Int()
    msgs_sent_uptime = fields.Int()
    msqs_recv_uptime = fields.Int()
    clients_connected = fields.Int()
    subscriptions_count = fields.Int()

class RssiHistorySchema(Schema):
    idRssi_history = fields.Int(dump_only=True)
    idBeacon = fields.Int()
#    controller = fields.Nested('ControllerSchema')
    rssi_value = fields.Int()
    distance = fields.Float()
    timestamp = fields.DateTime()

class ControllerStatusHistorySchema(Schema):
    idControllerStatusHistory = fields.Int(dump_only=True)
    cpu_avg = fields.Float()
    mem_used = fields.Float()
    mem_aval = fields.Float()
    bytes_sent = fields.Int()
    bytes_recv = fields.Int()
    uptime = fields.Int()
    processes_running = fields.Int()
#    controller = fields.Nested('ControllerSchema')
    timestamp = fields.DateTime()

class BeaconPositionHistorySchema(Schema):
    idBeaconPositionHistory = fields.Int(dump_only=True)
    position = fields.Nested('PositionSchema')
    beacon = fields.Nested('BeaconSchema')
    timestamp = fields.DateTime()
    correction_factor = fields.Float()
    controllers = fields.String()
    rssis = fields.String()
    distances = fields.String()
    
class SensorHistorySchema(Schema):
    idSensor_history = fields.Int(dump_only=True)
    sensor = fields.Nested('SensorSchema')
    timestamp = fields.DateTime()
    value = fields.Str()

class SmartDeviceSchema(DeviceSchema):
    
    idSmartDevice = fields.Int(dump_only=True)
    ip_address = fields.Str()
    statusdata = fields.Nested('SmartDeviceStatusSchema',many=True)
    
class SmartDeviceStatusSchema(Schema):
    idSmartDeviceStatus = fields.Int(dump_only=True)

    accelerometer = fields.Float()
    gyroscope = fields.Float()
    gps_lon = fields.Float()
    gps_lat = fields.Float()
    position = fields.Nested('PositionSchema')
    timestamp = Column(DateTime(), nullable=False)
    smartdevice = fields.Nested('SmartDeviceSchema')

#Base.metadata.create_all(engine)
