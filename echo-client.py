import requests
from six.moves import urllib

def make_request(host):#, api_key):
    """Makes a request to the auth info endpoint for Google ID tokens."""
    url = urllib.parse.urljoin(host, 'get_last_beacon_position_controllers')
#    params = {
#        'api_key':api_key
#    }
    body = {
            'beaconsId':'15,16,17',
            'controllers':'5,6,7,8,4',
            'to_date':'2017-11-15;18,0',
            'from_date':'2017-11-15;10,0'
            }


    response = requests.post(url,json=body)

    response.raise_for_status()
    return response.text


def main(host):#, api_key):
    response = make_request(host)#, api_key)
    print(response)


if __name__ == '__main__':
#    host = "http://test-api.endpoints.capitolcentrecourt.cloud.goog:8081/"	
#    api_key = "AIzaSyA986xMoSQZx_qrY604HCbEVCKXtJ48zy4" 
    host="http://127.0.0.1:8080/"

    main(host)#), api_key)

